


CC=clang++
CFLAGS=-c -Wall -std=c++11
LDFLAGS= -I/usr/local/include/SDL2 -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer  -stdlib=libstdc++ -pthread 
SOURCES= main.cpp directories.cpp tmxLoader.cpp tinyxml2.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=test

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@


clean:
	rm -rf *.o

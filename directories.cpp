#pragma once

#include "directories.hpp"
#include <stdexcept>
#include <iostream>

#ifdef DIRECTORY_H

namespace Directory
{
	std::string testFile(std::string &baseFilePath)
	{
		std::string fullFilePath = baseFilePath;
        std::string debugString = "Searching for: " + fullFilePath;


		std::ifstream tryFile(fullFilePath.c_str());
		if(tryFile.is_open())
		{
			debugString = "Found: " + fullFilePath;
			std::cout<< debugString << std::endl;
			return fullFilePath;
		}

		fullFilePath = LOCAL_ASSET_DIR + baseFilePath;
        debugString = "Searching for: " + fullFilePath;
		tryFile.open(fullFilePath.c_str());
		if(tryFile.is_open())
		{
			debugString = "Found: " + fullFilePath;
			std::cout<< debugString << std::endl;
			return fullFilePath;
		}

		throw std::runtime_error("File not found: " + fullFilePath);
	}
}

#endif //DIRECTORY_H

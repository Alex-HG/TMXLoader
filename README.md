TMXLoader
=========

A simple C++ parser and interpreter for the Tiled map editor. For some reason these are something that are very commonly used and lack proper tutorials or guides, so I took 20 mins to copy paste some code from my project to help anyone that may be having some trouble with this.


This is a very simple XML parser (thanks tinyXML2) that takes in a TMX file (thanks Tiled Map Editor) and outputs a container (any form the C++ STL will do) full of the correct tile IDs, the whole point is flexibility, it will make sure that no matter what changes the TMX file might have, the output will be correctly formated, equally important, most tutorials force a certian design pattern that you might now be using or at least like, this files are easily modified to fit your needs.


The directory files are just helper functions to make sure you correctly load the TMX file, feel free to change the /asset directory to whatever you use.


HOW TO USE:

1. Create an enum with the tile data you are using.
2. Make an instance of the resolveEnum template using said enum.
3. Thats it.

Included an example program that uses the map.tmx file.

![alt tag](http://i.imgur.com/ASizyxr.png)
![alt tag](http://i.imgur.com/Xr6xNLi.png)



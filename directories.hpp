#pragma once 

#include <string>
#include <fstream>
#include <exception>


#ifndef DIRECTORY_H
#define DIRECTORY_H

#ifdef _WIN32
#define LOCAL_ASSET_DIR "assets\\"
//const char *LOCAL_ASSET_DIR = "assets\\";
#endif 

#ifdef __linux__
#define LOCAL_ASSET_DIR "assets/"
//const char *LOCAL_ASSET_DIR = "assets/";
#endif

namespace Directory
{
	std::string testFile(std::string &filePath);
}


#endif //DIRECTORY_H

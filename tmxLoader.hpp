#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <memory>
#include "ID.hpp"
#include "enumParser.hpp"
#include "directories.hpp"

class TMXLoader
{
	int pHorizontalTiles, pVerticalTiles;
	std::list<int>pTileList;
	std::string pBackground;
	std::map<int, std::string> IDtoString;

public:
	
	TMXLoader(): pHorizontalTiles(0), pVerticalTiles(0), pBackground("") {};
	~TMXLoader();

	void loadFile(std::string file);
	
	int getLevelWidth();
	int getLevelHeigth();
	std::string getBackgroundName();
	std::list<int> getTileList();

	//this function is the real meat, after generating the ID map you pass, say a reference to the container of your tiles and generate them inside
	void generateLevel(std::list<int> &tiles);
};

